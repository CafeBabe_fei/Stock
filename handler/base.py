__author__ = 'fei'

import tornado.web
import configparser


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_cookie('user_name')

    def initialize(self):
        pass

    def on_finish(self):
        pass

    def write_error(self, status_code, **kwargs):
        if status_code == 404:
            self.render('404.html')
        elif status_code == 500:
            self.render('500.html')
        else:
            self.write('error:' + str(status_code))


class IndexHandler(BaseHandler):
    def get(self):
        self.render('index.html')







