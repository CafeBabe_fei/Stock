#coding:utf8


__author__ = 'fei'


import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import os
from tornado.options import define, options

from handler.stock import StockHandler
from handler.base import BaseHandler, IndexHandler

define('port', default=8000, help='run on the given port', type=int)

class Application(tornado.web.Application):
    def __init__(self):

        handlers = [(r'/', IndexHandler),
                    (r'/index', StockHandler),
                    (r".*", BaseHandler)]
        settings = {
            'static_path': os.path.join( os.path.dirname(__file__), 'static'),
            'template_path': os.path.join(os.path.dirname(__file__), 'templates'),
            'cookie_secret': 'LTUuWi7iImgJNDRdvAEB4beRGc/Qu=Wq=',
            'login_url': '/login',
            'autoreload': True
            }
        tornado.web.Application.__init__(self, handlers=handlers, **settings)

if __name__ == '__main__':
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer( Application(), xheaders=True )
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
